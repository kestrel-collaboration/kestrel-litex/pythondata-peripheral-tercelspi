// © 2017 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

// =============================================================================================
// Memory Map:
// =============================================================================================
// Device ID string (8 bytes)
// Base clock frequency (4 bytes)
// PHY configuration register 1 (4 bytes): {cs_extra_idle_cycles, 8'b0, dummy_cycle_count, spi_clock_divisor}
// ADC configuration register 1 (4 bytes): {controller_reset, 14'b0, input_range_select, adc_channel_enable}
// Channel 1 data (4 bytes): {4'b0, data}
// Channel 2 data (4 bytes): {4'b0, data}
// Channel 3 data (4 bytes): {4'b0, data}
// Channel 4 data (4 bytes): {4'b0, data}
// Channel 5 data (4 bytes): {4'b0, data}
// Channel 6 data (4 bytes): {4'b0, data}
// Channel 7 data (4 bytes): {4'b0, data}
// Channel 8 data (4 bytes): {4'b0, data}
// Channel 9 data (4 bytes): {4'b0, data}
// Channel 10 data (4 bytes): {4'b0, data}
// Channel 11 data (4 bytes): {4'b0, data}
// Channel 12 data (4 bytes): {4'b0, data}
// Channel 13 data (4 bytes): {4'b0, data}
// Channel 14 data (4 bytes): {4'b0, data}
// Channel 15 data (4 bytes): {4'b0, data}
// Channel 16 data (4 bytes): {4'b0, data}

// Stop LiteX silently ignoring net naming / missing register errors
`default_nettype none

module ads7950_spi_driver_wishbone(
		// Configuration registers
		input wire [31:0] sys_clk_freq,

		// Wishbone configuration port signals
		input wire wb_cyc,
		input wire wb_stb,
		input wire wb_we,
		input wire [29:0] wb_addr,
		input wire [31:0] wb_dat_w,
		output wire [31:0] wb_dat_r,
		input wire [3:0] wb_sel,
		output wire wb_ack,
		output wire wb_err,

		// SPI bus signals
		output wire spi_clock,
		output wire spi_d0_out,
		output wire spi_d0_direction,			// 0 == tristate (input), 1 == driven (output)
		input wire spi_d0_in,
		output wire spi_d1_out,
		output wire spi_d1_direction,			// 0 == tristate (input), 1 == driven (output)
		input wire spi_d1_in,
		output wire spi_ss_n,

		output wire [7:0] debug_port,

		input wire peripheral_reset,
		input wire peripheral_clock
	);

	// Control and status registers
	wire [63:0] device_id;
	wire [31:0] device_version;

	// PHY configuration register 1
	// Defaults to clock divisor 16
	reg [31:0] phy_cfg1 = 32'h00000010;

	// ADC configuration register 1
	// Defaults to all channels enabled
	reg [31:0] adc_cfg1 = 32'h0000ffff;

	// Sample registers
	reg [11:0] channel01_value = 0;
	reg [11:0] channel02_value = 0;
	reg [11:0] channel03_value = 0;
	reg [11:0] channel04_value = 0;
	reg [11:0] channel05_value = 0;
	reg [11:0] channel06_value = 0;
	reg [11:0] channel07_value = 0;
	reg [11:0] channel08_value = 0;
	reg [11:0] channel09_value = 0;
	reg [11:0] channel10_value = 0;
	reg [11:0] channel11_value = 0;
	reg [11:0] channel12_value = 0;
	reg [11:0] channel13_value = 0;
	reg [11:0] channel14_value = 0;
	reg [11:0] channel15_value = 0;
	reg [11:0] channel16_value = 0;

	// Device identifier
	assign device_id = 64'h7c52505441444346;
	assign device_version = 32'h00010000;

	reg wb_ack_reg = 0;
	reg [31:0] wb_dat_r_reg = 0;

	assign wb_ack = wb_ack_reg;
	assign wb_dat_r = wb_dat_r_reg;

	parameter WB_TRANSFER_STATE_IDLE = 0;
	parameter WB_TRANSFER_STATE_TR01 = 1;

	reg [31:0] wb_config_buffer_address_reg = 0;
	reg [7:0] wb_config_transfer_state = 0;
	reg [31:0] wb_cfg_space_tx_buffer = 0;
	reg [31:0] wb_cfg_space_rx_buffer = 0;

	// Wishbone connector
	always @(posedge peripheral_clock) begin
		if (peripheral_reset) begin
			// Reset Wishbone interface / control state machine
			wb_ack_reg <= 0;

			wb_config_transfer_state <= WB_TRANSFER_STATE_IDLE;
		end else begin
			case (wb_config_transfer_state)
				WB_TRANSFER_STATE_IDLE: begin
					// Compute effective address
					wb_config_buffer_address_reg[31:2] = wb_addr;
					case (wb_sel)
						4'b0001: wb_config_buffer_address_reg[1:0] = 0;
						4'b0010: wb_config_buffer_address_reg[1:0] = 1;
						4'b0100: wb_config_buffer_address_reg[1:0] = 2;
						4'b1000: wb_config_buffer_address_reg[1:0] = 3;
						4'b1111: wb_config_buffer_address_reg[1:0] = 0;
						default: wb_config_buffer_address_reg[1:0] = 0;
					endcase

					if (wb_cyc && wb_stb) begin
						// Configuration register space access
						// Single clock pulse signals in deasserted state...process incoming request!
						if (!wb_we) begin
							// Read requested
							case ({wb_config_buffer_address_reg[7:2], 2'b00})
								0: wb_cfg_space_tx_buffer = device_id[63:32];
								4: wb_cfg_space_tx_buffer = device_id[31:0];
								8: wb_cfg_space_tx_buffer = device_version;
								12: wb_cfg_space_tx_buffer = sys_clk_freq;
								16: wb_cfg_space_tx_buffer = phy_cfg1;
								20: wb_cfg_space_tx_buffer = adc_cfg1;
								24: wb_cfg_space_tx_buffer = channel01_value;
								28: wb_cfg_space_tx_buffer = channel02_value;
								32: wb_cfg_space_tx_buffer = channel03_value;
								36: wb_cfg_space_tx_buffer = channel04_value;
								40: wb_cfg_space_tx_buffer = channel05_value;
								44: wb_cfg_space_tx_buffer = channel06_value;
								48: wb_cfg_space_tx_buffer = channel07_value;
								52: wb_cfg_space_tx_buffer = channel08_value;
								56: wb_cfg_space_tx_buffer = channel09_value;
								60: wb_cfg_space_tx_buffer = channel10_value;
								64: wb_cfg_space_tx_buffer = channel11_value;
								68: wb_cfg_space_tx_buffer = channel12_value;
								72: wb_cfg_space_tx_buffer = channel13_value;
								76: wb_cfg_space_tx_buffer = channel14_value;
								80: wb_cfg_space_tx_buffer = channel15_value;
								84: wb_cfg_space_tx_buffer = channel16_value;
								default: wb_cfg_space_tx_buffer = 0;
							endcase

							// Endian swap
							wb_dat_r_reg[31:24] <= wb_cfg_space_tx_buffer[7:0];
							wb_dat_r_reg[23:16] <= wb_cfg_space_tx_buffer[15:8];
							wb_dat_r_reg[15:8] <= wb_cfg_space_tx_buffer[23:16];
							wb_dat_r_reg[7:0] <= wb_cfg_space_tx_buffer[31:24];

							// Signal transfer complete
							wb_ack_reg <= 1;

							wb_config_transfer_state <= WB_TRANSFER_STATE_TR01;
						end else begin
							// Write requested
							case ({wb_config_buffer_address_reg[7:2], 2'b00})
								// Device ID / version registers cannot be written, don't even try...
								16: wb_cfg_space_rx_buffer = phy_cfg1;
								20: wb_cfg_space_rx_buffer = adc_cfg1;
								// Sample registers cannot be written, don't even try...
								default: wb_cfg_space_rx_buffer = 0;
							endcase

							if (wb_sel[0]) begin
								wb_cfg_space_rx_buffer[7:0] = wb_dat_w[31:24];
							end
							if (wb_sel[1]) begin
								wb_cfg_space_rx_buffer[15:8] = wb_dat_w[23:16];
							end
							if (wb_sel[2]) begin
								wb_cfg_space_rx_buffer[23:16] = wb_dat_w[15:8];
							end
							if (wb_sel[3]) begin
								wb_cfg_space_rx_buffer[31:24] = wb_dat_w[7:0];
							end

							case ({wb_config_buffer_address_reg[7:2], 2'b00})
								16: phy_cfg1 <= wb_cfg_space_rx_buffer;
								20: adc_cfg1 <= wb_cfg_space_rx_buffer;
							endcase

							// Signal transfer complete
							wb_ack_reg <= 1;

							wb_config_transfer_state <= WB_TRANSFER_STATE_TR01;
						end
					end
				end
				WB_TRANSFER_STATE_TR01: begin
					// Cycle complete
					wb_ack_reg <= 0;
					wb_config_transfer_state <= WB_TRANSFER_STATE_IDLE;
				end
				default: begin
					// Should never reach this state
					wb_config_transfer_state <= WB_TRANSFER_STATE_IDLE;
				end
			endcase
		end
	end

	// SPI bus signals
	wire spi_data_direction;
	wire spi_quad_mode_pin_enable;
	wire drive_host_interfaces;
	assign drive_host_interfaces = 1;
	assign spi_d0_direction = !spi_quad_mode_pin_enable || (spi_data_direction & drive_host_interfaces);
	assign spi_d1_direction = spi_quad_mode_pin_enable && (spi_data_direction & drive_host_interfaces);

	// Interface registers
	wire [7:0] spi_clock_divisor;
	wire [7:0] dummy_cycle_count;
	wire [7:0] cs_extra_idle_cycles;
	wire [15:0] adc_channel_enable;
	wire input_range_select;
	wire controller_reset;
	assign spi_clock_divisor = phy_cfg1[7:0];
	assign dummy_cycle_count = phy_cfg1[15:8];
	assign cs_extra_idle_cycles = phy_cfg1[31:24];
	assign adc_channel_enable = adc_cfg1[15:0];
	assign input_range_select = adc_cfg1[16];
	assign controller_reset = adc_cfg1[31];

	parameter ADS7950_CMD_USE_PREV_MODE      = 4'b0000;
	parameter ADS7950_CMD_SET_MANUAL_MODE    = 4'b0001;
	parameter ADS7950_CMD_SET_AUTO1_MODE     = 4'b0010;
	parameter ADS7950_CMD_PROG_GPIO_CFG      = 4'b0100;
	parameter ADS7950_CMD_PROG_ALARM_GROUP_0 = 4'b1100;
	parameter ADS7950_CMD_PROG_ALARM_GROUP_1 = 4'b1101;
	parameter ADS7950_CMD_PROG_ALARM_GROUP_2 = 4'b1110;
	parameter ADS7950_CMD_PROG_ALARM_GROUP_3 = 4'b1101;

	// PHY clock generator and reset synchronizer
	// Divisor:
	// (spi_clock_divisor - 1) * 2
	// 0 == undefined (actually divide by two)
	// 1 == divide by 1
	// 2 == divide by 2
	// 3 == divide by 4
	// 4 == divide by 6
	// 5 == divide by 8
	// 6 == divide by 10
	// 7 == divide by 12
	// etc.
	(* noglobal *) reg phy_reset = 0;
	(* noglobal *) wire spi_phy_clock;
	(* noglobal *) reg spi_phy_clock_gen_reg = 0;
	(* noglobal *) reg spi_phy_clock_gen_reg_prev = 0;
	reg [7:0] spi_phy_clock_counter = 0;
	assign spi_phy_clock = (spi_clock_divisor == 1)?peripheral_clock:spi_phy_clock_gen_reg;
	always @(posedge peripheral_clock) begin
		// Clock generator
		if (spi_phy_clock_counter >= (spi_clock_divisor - 2)) begin
			spi_phy_clock_gen_reg <= ~spi_phy_clock_gen_reg;
			spi_phy_clock_counter <= 0;
		end else begin
			spi_phy_clock_counter <= spi_phy_clock_counter + 1;
		end

		// Reset synchronizer
		if ((spi_phy_clock_gen_reg_prev == 0) && (spi_phy_clock_gen_reg == 1)) begin
			phy_reset <= 0;
		end else begin
			if (peripheral_reset) begin
				phy_reset <= 1;
			end
		end

		spi_phy_clock_gen_reg_prev <= spi_phy_clock_gen_reg;
	end

	wire phy_ready;
	reg [31:0] phy_tx_data = 0;
	wire [31:0] phy_rx_data;
	reg phy_hold_ss_active = 0;
	reg phy_qspi_transfer_direction = 0;
	reg [7:0] phy_dummy_cycle_count = 0;
	reg [3:0] single_cycle_read_counter = 0;
	reg [3:0] single_cycle_write_counter = 0;
	reg phy_cycle_start = 0;
	wire phy_transaction_complete;

	spi_master_phy_quad spi_master_phy_quad(
		.platform_clock(spi_phy_clock),
		.reset(phy_reset),
		.ready(phy_ready),
		.tx_data(phy_tx_data),
		.rx_data(phy_rx_data),
		.dummy_cycle_count(phy_dummy_cycle_count),
		.hold_ss_active(phy_hold_ss_active),
		.idle_clock_polarity(1'b0),
		.spi_mode_assert_wp(1'b0),
		.spi_mode_assert_hold(1'b0),
		.qspi_mode_active(1'b0),
		.qspi_transfer_mode(2'b01),
		.qspi_transfer_direction(phy_qspi_transfer_direction),
		.cycle_start(phy_cycle_start),
		.transaction_complete(phy_transaction_complete),

		.spi_clock(spi_clock),
		.spi_d0_out(spi_d0_out),
		.spi_d0_in(spi_d0_in),
		.spi_d1_out(spi_d1_out),
		.spi_d1_in(spi_d1_in),
		.spi_ss_n(spi_ss_n),
		.spi_data_direction(spi_data_direction),
		.spi_quad_mode_pin_enable(spi_quad_mode_pin_enable)
	);

	reg [3:0] wb_sel_reg = 0;
	reg wb_ack_reg = 0;
	assign wb_ack = wb_ack_reg;
	reg [31:0] wb_dat_r_reg = 0;
	assign wb_dat_r = wb_dat_r_reg;

	parameter SPI_MASTER_TRANSFER_STATE_PHYI = 0;
	parameter SPI_MASTER_TRANSFER_STATE_DI01 = 1;
	parameter SPI_MASTER_TRANSFER_STATE_DI02 = 2;
	parameter SPI_MASTER_TRANSFER_STATE_IDLE = 3;
	parameter SPI_MASTER_TRANSFER_STATE_TR01 = 4;
	parameter SPI_MASTER_TRANSFER_STATE_TR02 = 5;
	parameter SPI_MASTER_TRANSFER_STATE_TR03 = 6;

	reg [7:0] spi_transfer_state = 0;
	reg [3:0] current_channel = 0;
	reg [7:0] cs_extra_cycle_counter = 0;
	reg [31:0] spi_cs_active_counter = 0;

	assign debug_port = spi_transfer_state;

	always @(posedge peripheral_clock) begin
		if (peripheral_reset || controller_reset) begin
			// Reset PHY
			phy_hold_ss_active <= 0;
			phy_qspi_transfer_direction <= 0;
			phy_dummy_cycle_count <= 0;
			phy_cycle_start <= 0;

			// Clear sample registers
			channel01_value <= 0;
			channel02_value <= 0;
			channel03_value <= 0;
			channel04_value <= 0;
			channel05_value <= 0;
			channel06_value <= 0;
			channel07_value <= 0;
			channel08_value <= 0;
			channel09_value <= 0;
			channel10_value <= 0;
			channel11_value <= 0;
			channel12_value <= 0;
			channel13_value <= 0;
			channel14_value <= 0;
			channel15_value <= 0;
			channel16_value <= 0;

			// Reset control state machine
			spi_cs_active_counter <= 0;
			spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_PHYI;
		end else begin
			case (spi_transfer_state)
				SPI_MASTER_TRANSFER_STATE_PHYI: begin
					if (phy_ready && (!phy_reset)) begin
						phy_tx_data[31:16] = 0;
						spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_DI01;
					end
				end
				SPI_MASTER_TRANSFER_STATE_DI01: begin
					// Set up SPI access
					phy_tx_data[15:12] = ADS7950_CMD_SET_MANUAL_MODE;
					phy_tx_data[11] = 1'b1;			// Set configuration bits
					phy_tx_data[10:7] = 0;			// Read channel 0 next
					phy_tx_data[6] = input_range_select;	// Set input range
					phy_tx_data[5] = 1'b0;			// Set normal operation
					phy_tx_data[4] = 1'b0;			// Transmit channel address with data
					phy_tx_data[3:0] = 4'b0;		// Set GPIO outputs to 0
					phy_dummy_cycle_count <= dummy_cycle_count;
					phy_hold_ss_active <= 0;
					phy_cycle_start <= 1;

					// Reset channel sequencer
					current_channel <= 0;

					spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_DI02;
				end
				SPI_MASTER_TRANSFER_STATE_DI02: begin
					if (phy_transaction_complete) begin
						phy_cycle_start <= 0;
						spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_IDLE;
					end
				end
				SPI_MASTER_TRANSFER_STATE_IDLE: begin
					// Start conversion process
					spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_TR01;
				end
				SPI_MASTER_TRANSFER_STATE_TR01: begin
					if (!phy_transaction_complete) begin
						if (!adc_channel_enable[current_channel]) begin
							// Skip channel
							spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_TR01;
						end else begin
							// Set up SPI access
							phy_tx_data[15:12] = ADS7950_CMD_SET_MANUAL_MODE;
							phy_tx_data[11] = 1'b0;			// Lock configuration bits
							phy_tx_data[10:7] = current_channel;
							phy_tx_data[6:0] = 0;			// Unused
							phy_dummy_cycle_count <= dummy_cycle_count;
							phy_hold_ss_active <= 0;
							phy_cycle_start <= 1;
	
							spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_TR02;
						end

						// Increment channel sequencer
						current_channel <= current_channel + 1;
					end
				end
				SPI_MASTER_TRANSFER_STATE_TR02: begin
					if (phy_transaction_complete) begin
						phy_cycle_start <= 0;

						// Decode response
						case (phy_rx_data[15:12])
							4'h0: channel01_value <= phy_rx_data[11:0];
							4'h1: channel02_value <= phy_rx_data[11:0];
							4'h2: channel03_value <= phy_rx_data[11:0];
							4'h3: channel04_value <= phy_rx_data[11:0];
							4'h4: channel05_value <= phy_rx_data[11:0];
							4'h5: channel06_value <= phy_rx_data[11:0];
							4'h6: channel07_value <= phy_rx_data[11:0];
							4'h7: channel08_value <= phy_rx_data[11:0];
							4'h8: channel09_value <= phy_rx_data[11:0];
							4'h9: channel10_value <= phy_rx_data[11:0];
							4'ha: channel11_value <= phy_rx_data[11:0];
							4'hb: channel12_value <= phy_rx_data[11:0];
							4'hc: channel13_value <= phy_rx_data[11:0];
							4'hd: channel14_value <= phy_rx_data[11:0];
							4'he: channel15_value <= phy_rx_data[11:0];
							4'hf: channel16_value <= phy_rx_data[11:0];
						endcase

						cs_extra_cycle_counter <= 0;
						spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_TR03;
					end
				end
				SPI_MASTER_TRANSFER_STATE_TR03: begin
					if (cs_extra_cycle_counter >= cs_extra_idle_cycles) begin
						spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_TR01;
					end else begin
						cs_extra_cycle_counter <= cs_extra_cycle_counter + 1;
						spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_TR03;
					end
				end
				default: begin
					spi_transfer_state <= SPI_MASTER_TRANSFER_STATE_DI01;
				end
			endcase
		end
	end
endmodule
